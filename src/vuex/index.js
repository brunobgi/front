import Vuex from 'vuex'

import preloader from './modules/preloader'
import auth from './modules/auth'
import user from './modules/user'
import client from './modules/client'
import product from './modules/product'
import opportunity from './modules/opportunity'

const store = new Vuex.Store({
    modules: {
        preloader,
        auth,
        user,
        client,
        product,
        opportunity,
    }
})

export default store
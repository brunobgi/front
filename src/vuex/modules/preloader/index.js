export default {
    state: {
        loading: false
    },

    mutations: {
        LOADING_STATUS(state, status) {
            state.loading = status
        }
    }
}
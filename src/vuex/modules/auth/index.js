import client from '@/services/http'

const key = 'TOKEN_AUTH'

export default {
    namespaced: true,

    state: {
        user: {}
    },

    mutations: {
        SET_USER(state, data) {
            state.user = data.user
        },

        REMOVE_USER(state) {
            state.user = {}
        }
    },

    actions: {
        login(context, params) {
            context.commit('LOADING_STATUS', true, { root: true })

            return new Promise((resolve, reject) => {
                client.post('/auth/login', params)
                    .then(response => {
                        context.commit('SET_USER', response.data)
                        localStorage.setItem(key, response.data.token)

                        resolve(response)
                    })
                    .catch(errors => reject(errors.response))
                    .finally(() => {
                        context.commit('LOADING_STATUS', false, { root: true })
                    })
            })
        },

        check(context) {
            return new Promise((resolve, reject) => {
                const token = localStorage.getItem(key)

                if (!token) return reject()

                client.post('/auth/me')
                    .then(response => {
                        context.commit('SET_USER', response.data)
                        resolve(response.data)
                    })
                    .catch(errors => reject(errors.response))
            })
        },

        me(context) {
            return new Promise((resolve, reject) => {
                const token = localStorage.getItem(key)

                if (!token) return reject()

                client.post('/auth/me')
                    .then(response => {
                        resolve(response.data)
                    })
                    .catch(errors => reject(errors.response))
            })
        },

        logout(context) {
            return new Promise((resolve, reject) => {
                const token = localStorage.getItem(key)

                if (!token) return reject()

                client.post('/auth/logout')
                    .then(response => {
                        context.commit('REMOVE_USER')
                        localStorage.removeItem(key)

                        resolve(response)
                    })
                    .catch(errors => reject(errors))
            })
        }
    },
}
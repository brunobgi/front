import client from '@/services/http'

export default {
    namespaced: true,

    state: {
        items: []
    },

    mutations: {
        LOAD_ITEMS(state, opportunities) {
            state.items = opportunities
        }
    },

    actions: {
        load(context, params) {
            context.commit('LOADING_STATUS', true, { root: true })

            return new Promise((resolve, reject) => {
                client.get('/opportunity', { params })
                    .then(response => {
                        context.commit('LOAD_ITEMS', response.data)
                    })
                    .catch(errors => reject(errors.response))
                    .finally(() => {
                        context.commit('LOADING_STATUS', false, { root: true })
                    })
            })
        },

        find(context, id) {
            context.commit('LOADING_STATUS', true, { root: true })

            return new Promise((resolve, reject) => {
                client.get(`/opportunity/${id}`)
                    .then(response => resolve(response))
                    .catch(errors => reject(errors.response))
                    .finally(() => context.commit('LOADING_STATUS', false, { root: true }))
            })
        },

        store(context, params) {
            context.commit('LOADING_STATUS', true, { root: true })

            return new Promise((resolve, reject) => {
                client.post('/opportunity', params)
                    .then(response => resolve(response))
                    .catch(errors => reject(errors.response))
                    .finally(() => context.commit('LOADING_STATUS', false, { root: true }))
            })
        },

        update(context, params) {
            context.commit('LOADING_STATUS', true, { root: true })

            return new Promise((resolve, reject) => {
                client.put(`/opportunity/${params.id}`, params)
                    .then(response => resolve(response))
                    .catch(errors => reject(errors.response))
                    .finally(() => context.commit('LOADING_STATUS', false, { root: true }));
            })
        },

        remove(context, id) {
            context.commit('LOADING_STATUS', true, { root: true })

            return new Promise((resolve, reject) => {
                client.delete(`/opportunity/${id}`)
                    .then(response => resolve(response))
                    .catch(errors => reject(errors.response))
            })
        }
    },
}
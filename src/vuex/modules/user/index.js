import client from '@/services/http'

export default {
    namespaced: true,

    state: {
        items: []
    },

    mutations: {
        LOAD_ITEMS(state, users) {
            state.items = users
        }
    },

    actions: {
        load(context, params) {

            return new Promise((resolve, reject) => {
                client.get('/user', { params })
                    .then(response => {
                        context.commit('LOAD_ITEMS', response.data)
                    })
                    .catch(errors => reject(errors.response))
            })
        },
    },
}
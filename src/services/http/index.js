import axios from 'axios'

const client = axios.create({
    baseURL: import.meta.env.VITE_API_URL,
    headers: {
        'content-type': 'application/json',
    },
})

client.interceptors.request.use((config) => {
    const token = localStorage.getItem('TOKEN_AUTH');

    if (token) config.headers.Authorization = `Bearer ${token}`
    return Promise.resolve(config)
})

export default client
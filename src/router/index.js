import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/login',
      name: 'login',
      component: () => import('../pages/Login/Index.vue'),
    },
    {
      path: '/',
      name: 'index',
      component: () => import('../pages/Opportunity/Index.vue'),
      meta: { auth: true },
    },
    {
      path: '/opportunity',
      name: 'opportunity',
      component: () => import('../pages/Opportunity/Index.vue'),
      meta: { auth: true },
    },
    {
      path: '/opportunity/create',
      name: 'opportunity.create',
      component: () => import('../pages/Opportunity/Create.vue'),
      meta: { auth: true },
    },
    {
      path: '/opportunity/:id/edit',
      name: 'opportunity.edit',
      component: () => import('../pages/Opportunity/Edit.vue'),
      props: true,
      meta: { auth: true },
    },
    {
      path: '/client',
      name: 'client',
      component: () => import('../pages/Client/Index.vue'),
      meta: { auth: true },
    },
    {
      path: '/client/create',
      name: 'client.create',
      component: () => import('../pages/Client/Create.vue'),
      meta: { auth: true },
    },
    {
      path: '/client/:id/edit',
      name: 'client.edit',
      component: () => import('../pages/Client/Edit.vue'),
      props: true,
      meta: { auth: true },
    },
    {
      path: '/product',
      name: 'product',
      component: () => import('../pages/Product/Index.vue'),
      meta: { auth: true },
    },
    {
      path: '/product/create',
      name: 'product.create',
      component: () => import('../pages/Product/Create.vue'),
      meta: { auth: true },
    },
    {
      path: '/product/:id/edit',
      name: 'product.edit',
      component: () => import('../pages/Product/Edit.vue'),
      props: true,
      meta: { auth: true },
    },
  ]
})

router.beforeEach((to, from, next) => {
  const token = localStorage.getItem('TOKEN_AUTH')

  if (to.meta.auth && !token) {
    next('/login')
  }

  next()
})

export default router

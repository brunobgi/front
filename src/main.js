import { createApp } from 'vue'
import Toast from "vue-toastification"

import App from './App.vue'
import router from './router'
import store from './vuex'

import Preloader from './components/shared/Preloader.vue'

// Bootstrap
import 'bootstrap'
import '@/sass/bootstrap.scss'

const app = createApp(App)

app.use(router)
app.use(store)

const options = {
    timeout: 3000
}

app.use(Toast, options)

// app.component('preloader', Preloader)

app.mount('#app')

store.dispatch('auth/check')
    .then(() => { })
    .catch(() => router.push({ name: 'login' }))

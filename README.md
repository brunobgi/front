# FRONTEND VITAMINAWEB

## Sobre o Projeto

Trata-se de um teste prático para a vaga de FullStack Sr. para a empresa Vitamina Web.<br>

Neste repositório encontra-se o código fonte do frontend desenvolvido em Vue.js + Bootstrap<br>

## Instalando o projeto

```sh
npm install
```

### Executando o projeto com Hot-Reload

```sh
npm run dev
```